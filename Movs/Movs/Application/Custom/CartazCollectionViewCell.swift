//
//  CartazCollectionViewCell.swift
//  Movs
//
//  Created by Anderson Silva on 13/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

protocol CartazCollectionViewCellDelegate : class {
    func addFavorite(in cell : CartazCollectionViewCell)
}

class CartazCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgCartaz: UIImageView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    
    weak var delegate : CartazCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(movie:MoviesResults, objFavorite:[FavoritesRealmDB]) {
        
        let urlImage = BaseURL.imgBaseURL + movie.posterPath!
        
        self.imgCartaz.kf.setImage(with: URL(string: urlImage), placeholder: nil, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        
        self.titleMovie.text = movie.title
        
        if objFavorite.count > 0 {
            
            let obj = objFavorite.filter{ $0.id == movie.id }
            
            if obj.count > 0 {
                movie.favorite = true
                self.btnFavorite.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
            }else{
                movie.favorite = false
                self.btnFavorite.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
            }
            
        }else {
            movie.favorite = false
            self.btnFavorite.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
        }
        
        
        
    }

    @IBAction func favoriteBtn(_ sender: Any) {
        self.delegate?.addFavorite(in: self)
    }
}

