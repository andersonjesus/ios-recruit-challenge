//
//  FavoritesTableViewCell.swift
//  Movs
//
//  Created by Anderson Silva on 14/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

class FavoritesTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCartaz: UIImageView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var yearMovie: UILabel!
    @IBOutlet weak var overviewMovie: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(favorite:FavoritesRealmDB) {
        let url = BaseURL.imgBaseURL + favorite.posterPath
        
        self.imgCartaz.kf.setImage(with: URL(string: url), placeholder: nil, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        self.titleMovie.text = favorite.title
        self.yearMovie.text = favorite.releaseDate.formatDate()
        self.overviewMovie.text = favorite.overview
    }
    
}
