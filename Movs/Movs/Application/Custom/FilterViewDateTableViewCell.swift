//
//  FilterViewDateTableViewCell.swift
//  Movs
//
//  Created by Anderson Silva on 14/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

class FilterViewDateTableViewCell: UITableViewCell {

    @IBOutlet weak var lbYear: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(years:Years) {
        self.lbYear.text = years.realeaseDate
    }

}
