//
//  BaseURL.swift
//  Movs
//
//  Created by Anderson Silva on 13/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation

struct BaseURL {
    static let baseURL = "https://api.themoviedb.org/3"
    static let imgBaseURL = "https://image.tmdb.org/t/p/w500"
}
