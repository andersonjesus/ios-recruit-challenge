//
//  BaseViewController.swift
//  Movs
//
//  Created by Anderson Silva on 09/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

protocol BaseViewInterface : class {
    func backModal()
    func backButton()
    func loadingShow()
    func loadingHide()
    func showViewError(title: String, msg: String, btn: String)
    func showNextVC(identifier:String, _ sender:Any?)
    func popViewController()
    func dismissModal(animated:Bool)
}

class BaseViewController: UIViewController {

    var loadingView : UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let recognizer:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(BaseViewController.backButton))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Métodos
    func addBackButton() {
        let barButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(BaseViewController.backButton))
        barButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButton
    }
    
    func addBackButtonModal() {
        let barButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(BaseViewController.backModal))
        barButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButton
    }    

}

extension BaseViewController : BaseViewInterface {
    
    @objc func backButton() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc func backModal() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadingShow() {
        self.loadingView = UIStoryboard(name: "Loading", bundle: nil).instantiateViewController(withIdentifier: "LoadingView")
        self.loadingView!.view.alpha = 0.0
        self.view.addSubview(self.loadingView!.view)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.loadingView!.view.alpha = 0.8
        })
    }
    func loadingHide() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.8, animations: {
                self.loadingView!.view.alpha = 0.0
            }, completion: { (finished) in
                if finished {
                    self.loadingView?.view.removeFromSuperview()
                    self.loadingView = nil
                }
            })
        }
    }
    func showViewError(title: String, msg: String, btn: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: btn, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showNextVC(identifier:String, _ sender:Any?) {
        self.performSegue(withIdentifier: identifier, sender: sender)
    }
    
    func popViewController() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func dismissModal(animated:Bool) {
        self.dismiss(animated: animated, completion: nil)
    }
}
