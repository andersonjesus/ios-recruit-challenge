//
//  RouterAPI.swift
//  Movs
//
//  Created by Anderson Silva on 13/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Alamofire

enum RouterAPI : URLRequestConvertible {
    
    case moviesPopular(param:Parameters)
    case genreList(param:Parameters)
    case movieDetail(movieID:Int)
    case movieRelease(year:Int)
    
    var header : HTTPHeaders {
        return ["Content-Type": "application/json"]
    }
    
    var api_key : String {
        return "api_key=fb8a7aa45890c02d6806f3a56abda32a"
    }
    
    var method : HTTPMethod {
        switch self {
            case .moviesPopular:
                return .get
            case .genreList:
                return .get
            case .movieDetail:
                return .get
            case .movieRelease:
                return .get
        }
    }
    
    var path : String {
        switch self {
            case .moviesPopular:
                return "/movie/popular/"
            case .genreList:
                return "/genre/movie/list/"
            case .movieDetail(let movieID):
                return "/movie/\(movieID)?" + api_key
            case .movieRelease(let year) :
                return "/discover/movie?primary_release_year=\(year)&" + api_key
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try BaseURL.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
//        urlRequest.allHTTPHeaderFields = header
        
        switch self {
            case .moviesPopular(let params):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: params)
            case .genreList(let params):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: params)
            case .movieDetail:
                urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            case .movieRelease :
                urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
//            default: break;
        }
        
        return urlRequest
    }
}
