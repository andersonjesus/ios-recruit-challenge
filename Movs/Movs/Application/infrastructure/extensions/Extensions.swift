//
//  Extensions.swift
//  Movs
//
//  Created by Anderson Silva on 09/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

extension String {
    func formatDate() -> String {
        let endingIndex = self.index(self.startIndex, offsetBy: 4)
        return String(self[..<endingIndex])
    }

    func removeLast(characters: Int) -> String {
        let endingIndex = self.index(self.endIndex, offsetBy: -characters)
        return String(self[..<endingIndex])
    }
}

extension UIColor {
    
    static func colorYellow() -> UIColor {
        return UIColor(red: 247/255.0, green: 206/255.0, blue: 91/255.0, alpha: 1.0)
    }
    
}
