//
//  FavoritesViewController.swift
//  Movs
//
//  Created by Anderson Silva on 14/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

class FavoritesViewController: BaseViewController {

    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var btnRemoveFilter: UIButton!
    
    
    let managerFavorite = FavoritesManager()
    var objFavorites = [FavoritesRealmDB]()
    var objFilter = [FavoritesRealmDB]()
    var years = [Years]()
    var dataFilter:String?
    var filter:Bool = false
    var total = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Movies"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !filter {
            self.loadingShow()
            self.objFavorites = self.managerFavorite.getMovieAll()
            self.total = self.objFavorites.count
            if self.objFavorites.count > 0 {
                for y in self.objFavorites {
                    self.years.append(Years(releaseData: y.releaseDate, checkIcon: false))
                }
                self.loadXib()
            }else{
                self.showViewError(title: "Movs", msg: "Sua lista está vázia!", btn: "OK")
            }
            self.loadingHide()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadXib() {
        
        self.tbView.delegate = self
        
        let tableNIB = UINib(nibName: "FavoritesTableViewCell", bundle: nil)
        self.tbView.register(tableNIB, forCellReuseIdentifier: "cellFavorite")
        self.tbView.tableHeaderView = self.btnRemoveFilter
        self.tbView.tableFooterView = UIView()
        self.tbView.reloadData()
        self.view.addSubview(self.tbView)
        
        
    }
    
    func refreshTotal() {
        let path = IndexPath(item: 0, section: 0)
        self.tbView.reloadRows(at: [path], with: .automatic)
    }

    @IBAction func viewFilter(_ sender: Any) {
        self.getViewFilter(years: self.years)
    }
    
    @IBAction func removeFilter(_ sender: Any) {
        self.objFilter.removeAll()
        self.objFavorites.removeAll()
        self.filter = false
        self.viewWillAppear(false)
    }
    
    func getViewFilter(years:[Years]) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc    = story.instantiateViewController(withIdentifier: "filtersViewController") as! FiltersViewController
        vc.filtroDelegate = self
        let nav   = UINavigationController(rootViewController: vc)
        nav.navigationBar.barTintColor = UIColor.colorYellow()
        nav.navigationBar.isTranslucent = false
        present(nav, animated: true, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FavoritesViewController : UITableViewDelegate, UITableViewDataSource, FilterDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "cellFavorite", for: indexPath) as! FavoritesTableViewCell
        if filter {
            cell.setup(favorite: self.objFilter[indexPath.row])
        }else{
            cell.setup(favorite: self.objFavorites[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var result : [UITableViewRowAction]?
        
        //Remove
        let removeAction = UITableViewRowAction(style: .default, title: "Remover", handler: { (action, indexPath) in
            self.managerFavorite.deleteMovie(self.objFavorites[indexPath.row])
            self.objFavorites.remove(at: indexPath.row)
            self.tbView.deleteRows(at: [indexPath], with: .automatic)
            if self.objFavorites.count > 0 {
                self.refreshTotal()
            }else{
                self.viewWillAppear(false)
            }
            if self.filter {
                self.managerFavorite.deleteMovie(self.objFilter[indexPath.row])
                self.objFilter.remove(at: indexPath.row)
                self.tbView.deleteRows(at: [indexPath], with: .automatic)
                self.refreshTotal()
            }
        })
        removeAction.backgroundColor = UIColor.red
        result = [removeAction]
        
        return result
    }
    
    func getData(data:String) {
        
        self.objFavorites.removeAll()
        self.objFilter.removeAll()
        self.filter = true
        self.objFilter = self.managerFavorite.getMovieFilter(value: data.formatDate())
        self.total = self.objFilter.count
        self.loadXib()
        
    }
    
}
