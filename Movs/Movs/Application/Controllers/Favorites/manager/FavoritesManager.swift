//
//  FavoritesManager.swift
//  Movs
//
//  Created by Anderson Silva on 14/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class FavoritesManager {
    
    let realm = try! Realm()    
    
    func saveMovie(_ movie : MoviesResults) -> Bool {
        
        var retorno = false
        
        let favorite = FavoritesRealmDB(movieResult: movie)
        try! self.realm.write {
            self.realm.add(favorite.self)
            retorno = true
        }
        
        return retorno
    }
    
    func getMovie(with id:Int) -> FavoritesRealmDB? {
        let movie = realm.object(ofType: FavoritesRealmDB.self, forPrimaryKey: id)
        return movie
    }
    
    func getMovieAll() -> [FavoritesRealmDB] {
        let movies : [FavoritesRealmDB] = realm.objects(FavoritesRealmDB.self).map { (movie) -> FavoritesRealmDB in
            return movie
        }
        return movies
    }
    
    func getMovieFilter(value:String) -> [FavoritesRealmDB] {
        
        let predicate = NSPredicate(format: "releaseDate contains %@", value)
        let movies : [FavoritesRealmDB] = realm.objects(FavoritesRealmDB.self).filter(predicate).map { (movie) -> FavoritesRealmDB in
            return movie
        }
        return movies
        
    }
    
    func deleteMovie(_ movie : FavoritesRealmDB) {
        try! realm.write {
            self.realm.delete(movie)
        }
    }
    
    func getArrayData() -> [Years] {
        
        var objYear = [Years]()
        var arrYear:[String] = []
        let favorites = self.getMovieAll()
        
        if favorites.count > 0 {
            for item in favorites {
                arrYear.append(item.releaseDate.formatDate())
            }
            
            let mySet = Set<String>(arrYear)
            arrYear = Array(mySet)
            arrYear.sort{ $0 < $1 }
            
            for i in arrYear {
                objYear.append(Years(releaseData: i, checkIcon: false))
            }
        }
        
        return objYear
    }
    
}
