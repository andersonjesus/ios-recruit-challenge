//
//  MoviesBusiness.swift
//  Movs
//
//  Created by Anderson Silva on 09/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation

class MoviesBusiness {
    
    lazy var provider : MoviesProvider = {
        return MoviesProvider.init()
    }()
    
    private var page = 1
    
    func getMovies(completion:@escaping(_ movies:[MoviesResults]?, _ totalCount:Int?, _ error:String?) -> Void) -> Void {
        
        provider.getMovies(page: page) { (moviesBase, error) in
            guard error == nil else {
                completion(nil, nil, error?.localizedDescription)
                return
            }
            
            self.page = self.page + 1
            completion(moviesBase?.results, moviesBase?.totalResults, nil)
        }
    }
    
    func getMovieDetail(movieID:Int, completion:@escaping(_ movie:MovieDetailBaseClass?, _ error:String?) -> Void) -> Void {
        
        provider.getMovieDetail(movieID: movieID) { (movie, error) in
            guard error == nil else {
                completion(nil, error?.localizedDescription)
                return
            }
            completion(movie, nil)
        }
        
    }
    
    func getMovieGenres(completion:@escaping(_ genre:MovieDetailGenres?, _ error:String?) -> Void) -> Void {
        
        provider.getMovieGenres { (genre, error) in
            guard error == nil else {
                completion(nil, error?.localizedDescription)
                return
            }
            completion(genre, nil)
        }
        
    }
    
}


