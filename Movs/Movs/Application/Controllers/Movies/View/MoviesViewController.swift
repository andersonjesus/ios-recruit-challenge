//
//  MoviesViewController.swift
//  Movs
//
//  Created by Anderson Silva on 09/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

class MoviesViewController: BaseViewController {

    var totalResult = 0
    let managerFavorite = FavoritesManager()
    let manager = MoviesManager()
    var movies = [MoviesResults]()
    var objFavorites = [FavoritesRealmDB]()
    
    @IBOutlet weak var collection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Movies"
        self.manager.baseInterface = self
        self.manager.moviesInterface = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        manager.getMovies()
        self.objFavorites = managerFavorite.getMovieAll()        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadXib() {
        
        let collectionNIB = UINib(nibName: "CartazCollectionViewCell", bundle: nil)
        self.collection.register(collectionNIB, forCellWithReuseIdentifier: "cellCartaz")
        self.view.addSubview(self.collection)
        self.collection.reloadData()
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sgMovieDetail" {
            
            let vc = segue.destination as! MovieDetailViewController
//            let obj = sender as! MoviesResults
//            vc.movieID = obj.id
//            vc.favorite = obj.favorite
            vc.movieResult = sender as! MoviesResults
            
        }
        
    }
 

}


extension MoviesViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CartazCollectionViewCellDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if ((self.movies.count < self.totalResult) && (self.movies.count-1 == indexPath.row)){
            self.manager.getMovies()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionUsableWidth = collectionView.frame.width - 60
        return CGSize(width: collectionUsableWidth/2, height: collectionUsableWidth/1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collection.dequeueReusableCell(withReuseIdentifier: "cellCartaz", for: indexPath) as! CartazCollectionViewCell
        cell.delegate = self
        cell.setup(movie: self.movies[indexPath.row], objFavorite: self.objFavorites)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.showNextVC(identifier: "sgMovieDetail", self.movies[indexPath.row])
    }
    
    func addFavorite(in cell : CartazCollectionViewCell) {
        if let indexPath = self.collection.indexPath(for: cell) {
            if !self.movies[indexPath.row].favorite! {
                if self.managerFavorite.saveMovie(self.movies[indexPath.row]) {
                    self.movies[indexPath.row].favorite = true
                    cell.btnFavorite.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
                }
            }
        }
    }
    
}

extension MoviesViewController : MoviesInterface {
    
    func updateMovies(totalCount:Int, movies:[MoviesResults]) {
        
        self.totalResult = totalCount
        self.movies = movies
        self.loadXib()
        
    }
    
    func fetchMovie(movie:MovieDetailBaseClass, generString:[String]) { }
}
