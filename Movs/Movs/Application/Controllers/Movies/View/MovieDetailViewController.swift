//
//  MovieDetailViewController.swift
//  Movs
//
//  Created by Anderson Silva on 13/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

class MovieDetailViewController: BaseViewController {
    
//    var movieID:Int!
    var movieResult:MoviesResults!
    
    let manager = MoviesManager()
    let managerFavorite = FavoritesManager()
    var movie:MovieDetailBaseClass?
    var objFavorites = [FavoritesRealmDB]()
    
    @IBOutlet weak var imgCartaz: UIImageView!
    @IBOutlet weak var titleCartaz: UILabel!
    @IBOutlet weak var lbYear: UILabel!
    @IBOutlet weak var gener: UILabel!
    @IBOutlet weak var lbOverview: UITextView!
    @IBOutlet weak var btnFavorite: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadingShow()
        self.addBackButton()
        self.manager.getMovieDetail(movieID: self.movieResult.id!)
        
        self.manager.baseInterface = self
        self.manager.moviesInterface = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.objFavorites = self.managerFavorite.getMovieAll()
        
        if self.objFavorites.count > 0 {
            let obj = objFavorites.filter{ $0.id == self.movieResult.id }
            if obj.count > 0 {
                self.movieResult.favorite = true
            }else{
                self.movieResult.favorite = false
                self.btnFavorite.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
            }
        }else{
            self.movieResult.favorite = false
            self.btnFavorite.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
        }
    }
    
    @IBAction func favoriteBtn(_ sender: Any) {
        
        if !self.movieResult.favorite! {
            if managerFavorite.saveMovie(self.movieResult) {
                self.movieResult.favorite = true
                self.btnFavorite.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MovieDetailViewController : MoviesInterface {
    
    func updateMovies(totalCount:Int, movies:[MoviesResults]) { }
    func fetchMovie(movie:MovieDetailBaseClass, generString:[String]) {

        let urlImage = BaseURL.imgBaseURL + movie.posterPath!
    
        self.imgCartaz.kf.setImage(with: URL(string: urlImage), placeholder: nil, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        self.title = movie.title!
        self.titleCartaz.text = movie.title!
        self.lbYear.text = movie.releaseDate!.formatDate()
        
        self.gener.numberOfLines = generString.count
        self.gener.text = generString.joined(separator: ", ")
        
        self.lbOverview.text = movie.overview!
        self.loadingHide()
    }
}
