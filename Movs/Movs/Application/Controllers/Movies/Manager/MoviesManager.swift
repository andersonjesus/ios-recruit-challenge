//
//  MoviesManager.swift
//  Movs
//
//  Created by Anderson Silva on 09/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation

protocol MoviesInterface : class {
    func updateMovies(totalCount:Int, movies:[MoviesResults])
    func fetchMovie(movie:MovieDetailBaseClass, generString:[String])
}

class MoviesManager {
    
    lazy var business : MoviesBusiness = {
        return MoviesBusiness.init()
    }()
    
    var objMovies = [MoviesResults]()
    
    weak var moviesInterface : MoviesInterface?
    weak var baseInterface : BaseViewInterface?
    
    func getMovies() {
        
        self.baseInterface?.loadingShow()
        business.getMovies { (movies, totalResults, error) in
            
            self.baseInterface?.loadingHide()
            
            guard error == nil else {
                self.baseInterface?.showViewError(title: "Ops!", msg: error!, btn: "OK")
                return
            }
            
            if movies!.count > 0 {
                for m in movies! {
                    self.objMovies.append(m)
                }
                self.moviesInterface?.updateMovies(totalCount: totalResults!, movies: self.objMovies)
            }else{
                self.baseInterface?.showViewError(title: "Ops!", msg: "Tente novamente", btn: "OK")
                return
            }
            
            
        }
        
    }
    
    func getMovieDetail(movieID:Int) {
        
        business.getMovieDetail(movieID: movieID) { (movie, error) in
            
            var generString : [String] = []
            
            guard error == nil else {
                self.baseInterface?.showViewError(title: "Ops!", msg: error!, btn: "OK")
                return
            }
            
            for genres in (movie?.genres!)! {
                generString.append(genres.name!)
            }
            self.moviesInterface?.fetchMovie(movie: movie!, generString: generString)
        }
    }
    
}
