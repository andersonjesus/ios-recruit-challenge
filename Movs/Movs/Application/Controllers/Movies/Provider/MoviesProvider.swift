//
//  MoviesProvider.swift
//  Movs
//
//  Created by Anderson Silva on 09/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


class MoviesProvider : NSObject {
    
    private let param = ["api_key":"fb8a7aa45890c02d6806f3a56abda32a"] as [String : Any]
    
    func getMovies(page:Int, completion:@escaping(_ movieBase:MoviesBaseClass?, _ error:Error?) -> Void) -> Void {
        
        let params = ["api_key":"fb8a7aa45890c02d6806f3a56abda32a",
                     "page":page] as [String : Any]
        
        request(RouterAPI.moviesPopular(param: params))
            .validate(contentType: ["application/json"])
            .responseObject { (response:DataResponse<MoviesBaseClass>) in
                switch response.result {
                    case .success(let movie): completion(movie, nil); break;
                    case .failure(let error): completion(nil, error); break;
                }
        }
        
    }
    
    func getMovieDetail(movieID:Int, completion:@escaping(_ movie:MovieDetailBaseClass?, _ error:Error?) -> Void) -> Void {
        
        let url = BaseURL.baseURL + "/movie/\(movieID)?api_key=fb8a7aa45890c02d6806f3a56abda32a"
        
        request(URL(string: url)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate(contentType: ["application/json"])
            .responseObject { (response:DataResponse<MovieDetailBaseClass>) in
                switch response.result {
                    case .success(let movie): completion(movie, nil); break;
                    case .failure(let error): completion(nil, error); break;
                }
        }
        
        
    }
    
    func getMovieGenres(completion:@escaping(_ genre:MovieDetailGenres?, _ error:Error?) -> Void) -> Void {
        
        request(RouterAPI.genreList(param: self.param))
            .validate(contentType: ["application/json"])
            .responseObject { (response:DataResponse<MovieDetailGenres>) in
                switch response.result {
                    case .success(let genre) : completion(genre, nil); break;
                    case .failure(let error) : completion(nil, error); break;
                }
        }
        
        
    }
    
}
