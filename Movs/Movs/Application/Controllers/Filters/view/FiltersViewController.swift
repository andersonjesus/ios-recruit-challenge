//
//  FiltersViewController.swift
//  Movs
//
//  Created by Anderson Silva on 14/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

protocol FilterDelegate : class {
    func getData(data:String)
}

class FiltersViewController: BaseViewController {

    var objYear:Years?
    
    @IBOutlet weak var tbView: UITableView!
    
    weak var filtroDelegate : FilterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Filter"
        self.addBackButtonModal()
        self.tbView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func apply(_ sender: Any) {
        if self.objYear != nil {
            self.filtroDelegate?.getData(data: (self.objYear?.realeaseDate)!)
            self.backModal()
        }
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "sgDate" {
            let vc = segue.destination as! FilterYearsViewController
            vc.delegateFilter = self
        }
        
    }
 

}
extension FiltersViewController : UITableViewDelegate, UITableViewDataSource, FilterYearDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "cellDate") as! FilterViewDateTableViewCell
        if self.objYear != nil {
            cell.setup(years: self.objYear!)
        }
        return cell
    }
    
    func getDate(years:Years) {
        self.objYear = years
        self.tbView.reloadData()
    }
}
