//
//  FilterYearsViewController.swift
//  Movs
//
//  Created by Anderson Silva on 14/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import UIKit

protocol FilterYearDelegate : class {
    func getDate(years:Years)
}

class FilterYearsViewController: BaseViewController {

    var objYear = [Years]()    
    var manager = FavoritesManager()
    
    @IBOutlet weak var tbView: UITableView!
    
    weak var delegateFilter : FilterYearDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tbView.delegate = self
        self.tbView.dataSource = self
        
        self.addBackButton()
        self.tbView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.objYear = self.manager.getArrayData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FilterYearsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objYear.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "cellYear", for: indexPath) as! FilterYearsTableViewCell
        cell.setup(year: self.objYear[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.objYear[indexPath.row].checkIcon = true
        self.delegateFilter?.getDate(years: self.objYear[indexPath.row])
        self.backButton()
    }
    
}
