//
//  FavoritesRealmDB.swift
//  Movs
//
//  Created by Anderson Silva on 14/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation
import RealmSwift

class FavoritesRealmDB : Object {
    
    @objc dynamic var posterPath: String = ""
    @objc dynamic var backdropPath: String = ""
    @objc dynamic var voteCount: Int = 0
    @objc dynamic var overview: String = ""
    @objc dynamic var originalTitle: String = ""
    @objc dynamic var popularity: Float = 0
    @objc dynamic var releaseDate: String = ""
    @objc dynamic var id: Int = 0
    @objc dynamic var video: Bool = false
    @objc dynamic var originalLanguage: String = ""
    @objc dynamic var voteAverage: Float = 0
    @objc dynamic var title: String = ""
    @objc dynamic var adult: Bool = false
//    @objc dynamic var favorite : Bool = false
    
    func primaryKey() -> Int {
        return id
    }
    
    convenience init(movieResult:MoviesResults) {
        self.init()
        self.posterPath = movieResult.posterPath!
        self.backdropPath = movieResult.backdropPath!
        self.voteCount = movieResult.voteCount!
        self.overview = movieResult.overview!
        self.originalTitle = movieResult.originalTitle!
        self.popularity = movieResult.popularity!
        self.releaseDate = movieResult.releaseDate!
        self.id = movieResult.id!
        self.video = movieResult.video!
        self.originalTitle = movieResult.originalTitle!
        self.voteAverage = movieResult.voteAverage!
        self.title = movieResult.title!
        self.adult = movieResult.adult!
//        self.favorite = movieResult.favorite!
    }
    
}


