//
//  MovieDetailSpokenLanguages.swift
//
//  Created by Anderson Silva on 09/02/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class MovieDetailSpokenLanguages: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iso6391 = "iso_639_1"
    static let name = "name"
  }

  // MARK: Properties
  public var iso6391: String?
  public var name: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    iso6391 <- map[SerializationKeys.iso6391]
    name <- map[SerializationKeys.name]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = iso6391 { dictionary[SerializationKeys.iso6391] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.iso6391 = aDecoder.decodeObject(forKey: SerializationKeys.iso6391) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(iso6391, forKey: SerializationKeys.iso6391)
    aCoder.encode(name, forKey: SerializationKeys.name)
  }

}
