//
//  Years.swift
//  Movs
//
//  Created by Anderson Silva on 14/02/2018.
//  Copyright © 2018 Anderson Silva. All rights reserved.
//

import Foundation

struct Years {
    var realeaseDate : String
    var checkIcon:Bool
    
    init(releaseData:String, checkIcon:Bool) {
        self.realeaseDate = releaseData
        self.checkIcon = checkIcon
    }
    
}
