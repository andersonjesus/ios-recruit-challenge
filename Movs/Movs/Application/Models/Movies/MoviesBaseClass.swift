//
//  MoviesBaseClass.swift
//
//  Created by Anderson Silva on 09/02/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class MoviesBaseClass: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let totalResults = "total_results"
    static let page = "page"
    static let results = "results"
    static let totalPages = "total_pages"
  }

  // MARK: Properties
  public var totalResults: Int?
  public var page: Int?
  public var results: [MoviesResults]?
  public var totalPages: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    totalResults <- map[SerializationKeys.totalResults]
    page <- map[SerializationKeys.page]
    results <- map[SerializationKeys.results]
    totalPages <- map[SerializationKeys.totalPages]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = totalResults { dictionary[SerializationKeys.totalResults] = value }
    if let value = page { dictionary[SerializationKeys.page] = value }
    if let value = results { dictionary[SerializationKeys.results] = value.map { $0.dictionaryRepresentation() } }
    if let value = totalPages { dictionary[SerializationKeys.totalPages] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.totalResults = aDecoder.decodeObject(forKey: SerializationKeys.totalResults) as? Int
    self.page = aDecoder.decodeObject(forKey: SerializationKeys.page) as? Int
    self.results = aDecoder.decodeObject(forKey: SerializationKeys.results) as? [MoviesResults]
    self.totalPages = aDecoder.decodeObject(forKey: SerializationKeys.totalPages) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(totalResults, forKey: SerializationKeys.totalResults)
    aCoder.encode(page, forKey: SerializationKeys.page)
    aCoder.encode(results, forKey: SerializationKeys.results)
    aCoder.encode(totalPages, forKey: SerializationKeys.totalPages)
  }

}
